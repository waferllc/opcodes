//************************
// recv function for code  os dependatn
// for now only Linux implementation
// 7-14-2020
// sergey Uskach




#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 


#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */

#include "opcodes.h"

/************************************************************************
* Name       : RC comm_pushTxBuffer(CommTxAsciiBuff *CommTxAsciiBuffer)
* Description:
*
*
* Note(s)    :
************************************************************************/
int  comm_pushTxBuffer(COMM_TYPE_DEFINE CommType, char DacNum, unsigned char *pStr, unsigned int length , int  clientfd )
{
    int rc =0;
    //unsigned short calcCRC;
    if(!pStr)
        return(-1);
#if 0
    //Transmit threw USB.
    if(CommType == COMM_USB)
    {
        USBCDCD_sendData(pStr, length, BIOS_WAIT_FOREVER);
        UsbCommunication = BIT_USB_COMMUNICATION;
    }



    //Transmit threw SPI.
    else if(CommType == COMM_SPI){
        //SPIdrv_Tx (DacNum, pStr,length);
        SPIdrv_programDAC(DacNum, pStr, length);            //David C. - Uses TIRTOS SPI drivers
        SpiCommunication = BIT_SPI_COMMUNICATION;
    }

    //Transmit threw UART.
    else if(CommType == COMM_UART)
    {
        //calculate new checkSum and store in bytes [10] & [11]
        calcCRC = crc(pStr, length);
        pStr[10] = (calcCRC & 0xff00) >> 8;
        pStr[11] = calcCRC & 0x00ff;
        UART_write(uartOpcode, pStr, length);

    }
#endif	
    //Transmit threw UDP.
    else if(CommType == COMM_UDP)
    {
    }

    //Transmit threw I2C.
    else if(CommType == COMM_I2C)
    {
    }

    //Transmit threw I2CTCP.
    else if(CommType == COMM_TCP)
    {
        if ( clientfd != 0 )
            rc = send(clientfd, pStr, length, 0);

        //TcpCommunication = BIT_TCP_COMMUNICATION;
    }

    return(rc);
}