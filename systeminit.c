//************************
// system init code  os dependatn
// for now only Linux implementation
// 7-14-2020
// sergey Uskach



#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 
#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */


#include "opcodes.h"

unsigned char buffer[TCPPACKETSIZE] ={0};  

int counter; 
// this is for now just stub 

//recv tred id;
void  recvthreadId( void *);

void Board_initGeneral(void)
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}
void Board_initGPIO(void )
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}

void Board_initEMAC(void )
{
	
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}
	
void Board_initUSB(int usb_dev)
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}

void IOports_Init(void)
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}

void Board_initUART(void )
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}


void I2C_InitGps(void)
{
	printf ( "%s  %s %d\n", __func__, __FILE__, __LINE__ );
	return;
}


struct termios new_port_settings,
       old_port_settings;

int OpenComport(char *comport, int baudrate)
{
#if 0
  int baudr, Cport;

  switch(baudrate)
  {
    case      50 : baudr = B50;
                   break;
    case      75 : baudr = B75;
                   break;
    case     110 : baudr = B110;
                   break;
    case     134 : baudr = B134;
                   break;
    case     150 : baudr = B150;
                   break;
    case     200 : baudr = B200;
                   break;
    case     300 : baudr = B300;
                   break;
    case     600 : baudr = B600;
                   break;
    case    1200 : baudr = B1200;
                   break;
    case    1800 : baudr = B1800;
                   break;
    case    2400 : baudr = B2400;
                   break;
    case    4800 : baudr = B4800;
                   break;
    case    9600 : baudr = B9600;
                   break;
    case   19200 : baudr = B19200;
                   break;
    case   38400 : baudr = B38400;
                   break;
    case   57600 : baudr = B57600;
                   break;
    case  115200 : baudr = B115200;
                   break;
    case  230400 : baudr = B230400;
                   break;
    case  460800 : baudr = B460800;
                   break;
    case  500000 : baudr = B500000;
                   break;
    case  576000 : baudr = B576000;
                   break;
    case  921600 : baudr = B921600;
                   break;
    case 1000000 : baudr = B1000000;
                   break;
    default      : printf("invalid baudrate\n");
                   return(1);
                   break;
  }

  Cport = open(comport, O_RDWR | O_NOCTTY | O_NDELAY);
  if(Cport==-1)
  {
    perror("unable to open comport ");
    return(-1);
  }

  error = tcgetattr(Cport, &old_port_settings);
  if(error==-1)
  {
    close(Cport);
    perror("unable to read portsettings ");
    return(-1);
  }
  memset(&new_port_settings, 0, sizeof(new_port_settings));  /* clear the new struct */

  new_port_settings.c_cflag = baudr | CS8 | CLOCAL | CREAD;
  new_port_settings.c_iflag = IGNPAR;
  new_port_settings.c_oflag = 0;
  new_port_settings.c_lflag = 0;
  new_port_settings.c_cc[VMIN] = 0;      /* block untill n bytes are received */
  new_port_settings.c_cc[VTIME] = 0;     /* block untill a timer expires (n * 100 mSec.) */
  error = tcsetattr(Cport, TCSANOW, &new_port_settings);
  if(error==-1)
  {
    close(Cport);
    perror("unable to adjust portsettings ");
    return(-1);
  }
#endif

  return(0);
}

#define NUMTCPWORKERS 3

void  recvthreadId ( void *t_param )
{

    int 					rc;
    int 					bytesRcvd;
    fd_set					set;
    struct 					timeval timeout;    
    char  					Opcode ;
    int 					accepted_clientfd;
    
	memcpy (&accepted_clientfd ,t_param, sizeof (int ) ); 
    FD_ZERO(&set); /* clear the set */
    FD_SET(accepted_clientfd  , &set); /* add our file descriptor to the set */
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;

  
    while ((rc = select(accepted_clientfd  , &set, NULL, NULL, &timeout)) >= 0)
    {
        
        bytesRcvd = recv(accepted_clientfd , buffer, TCPPACKETSIZE, 0) ;
        
        if (bytesRcvd == SOCKET_ERROR)
        {
            if (errno !=  EWOULDBLOCK )
            {
                printf("ERROR: CLosing errno=%d\n", errno);
                FD_CLR(accepted_clientfd  , &set);
                printf(" CLosing client: 0x%x link is down __LINE__ %d\n", accepted_clientfd , __LINE__);
                close(accepted_clientfd);
                return;
            }
            
        }
        else if (bytesRcvd == 0)
        {

            printf(" bytesRcvd == 0 CLosing client: %d __LINE__ %d\n", accepted_clientfd , __LINE__ );
            FD_CLR(accepted_clientfd  , &set);
            close(accepted_clientfd );
            return ;
        }
        else
        {
            //get the opcode from recieve byte 7.
            Opcode = buffer[OPCODE2];
            Communication(COMM_TCP, Opcode , buffer, accepted_clientfd  );
        }


    }

    if ( rc < 0)
    {
        printf("rc=%d  errno %d  line = %d\n", rc,errno, __LINE__);
    }

    if (bytesRcvd < 0  )
	{

        printf("bytesRcvd < 0 ERROR: CLosing client: %d __LINE__ %d\n", errno, __LINE__);
        
        printf("connection_count=%d  line %d\n", connection_count, __LINE__);
        
    }
		
	printf(" CLosing client: 0x%x errno=%d __LINE__ %d\n", accepted_clientfd , errno, __LINE__);

    close(accepted_clientfd );

   
    return ;

}


void tcpip_task ( void *t_param )
{

int  				clientfd;
int                	status;
int                	server;
int  				server_port;
struct sockaddr_in 	serverAddr;
struct sockaddr_in 	clientAddr;
struct linger      	linger_opts;
int                	optval;
int                	optlen = sizeof(optval);
socklen_t          	addrlen = sizeof(clientAddr);
T_Params  			*t_params = (T_Params	*)t_param;
	


    server = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (server == -1) 
	{        
        printf("Error: socket not created error %d.\n", errno); 
        
        return;
    }
		
	//memset(&serverAddr, 0, sizeof(serverAddr));
	bzero((char *) &serverAddr, sizeof(serverAddr));
	server_port = t_params->port ;
	
    serverAddr.sin_family = AF_INET;    
	
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddr.sin_port = htons(server_port);

    status = bind(server, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (status == -1) {
		close ( server );
        return;
    }

    status = listen(server, NUMTCPWORKERS);
    if (status == -1)
	{
        printf("Error: listen failed.\n");
        close ( server );
        return;
    }

    optval = 1;
    if (setsockopt(server, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0)
	{
        printf("Error: setsockopt failed\n");
        close ( server );
        return;
    }

    while ((clientfd =
            accept(server, (struct sockaddr *)&clientAddr, &addrlen)) != -1) 
	{

			linger_opts.l_onoff = 1;
			linger_opts.l_linger = 0;
			if( setsockopt(clientfd, SOL_SOCKET, SO_LINGER, &linger_opts, sizeof(linger_opts)) < 0 )
			{
				printf("Error: setsockopt SO_LINGER, failed\n");
				close ( server );
				return;
			}

			//prepare and start recv task
		
			// Create a thread that will function threadFunc()

			recvthreadId (  (void *)&clientfd );
        
			
    
  
		
	}
	return;
}
