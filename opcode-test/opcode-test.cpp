
// Client program example

#define WIN32_LEAN_AND_MEAN

#if (_MSC_VER >= 1915)
#define no_init_all deprecated
#endif

#include <winsock2.h>

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include "opcodes.h"


//#define DEFAULT_PORT 2007

// TCP socket type

#define DEFAULT_PROTO SOCK_STREAM






int main(int argc, char **argv)

{

	char Buffer[2048] = { 0  };
	// default to localhost

	char *server_name = (char *)DEFAULT_IP;

	unsigned short port = DEFAULT_PORT;

	int retval, loopflag = 0;

	int loopcount, maxloop = -1;

	int socket_type = DEFAULT_PROTO;

	struct sockaddr_in server;

	//struct hostent *hp;

	WSADATA wsaData;

	SOCKET  conn_socket;


	socket_type = SOCK_STREAM;

	
	if ((retval = WSAStartup(0x202, &wsaData)) != 0)

	{

		fprintf(stderr, "Client: WSAStartup() failed with error %d\n", retval);

		WSACleanup();

		return -1;

	}

	else

		printf("Client: WSAStartup() is OK.\n");



	// Attempt to detect if we should call gethostbyname() or gethostbyaddr()

	// Copy the resolved information into the sockaddr_in structure

	memset(&server, 0, sizeof(server));
#pragma warning(suppress : 4996)	
	server.sin_addr.s_addr  = inet_addr(server_name);
	server.sin_family = AF_INET;
	server.sin_port = htons(port);



	conn_socket = socket(AF_INET, socket_type, 0); /* Open a socket */

	if (conn_socket < 0)

	{

		fprintf(stderr, "Client: Error Opening socket: Error %d\n", WSAGetLastError());

		WSACleanup();

		return -1;

	}

	else

		printf("Client: socket() is OK.\n");



	// Notice that nothing in this code is specific to whether we

	// are using UDP or TCP.

	// We achieve this by using a simple trick.

	//    When connect() is called on a datagram socket, it does not

	//    actually establish the connection as a stream (TCP) socket

	//    would. Instead, TCP/IP establishes the remote half of the

	//    (LocalIPAddress, LocalPort, RemoteIP, RemotePort) mapping.

	//    This enables us to use send() and recv() on datagram sockets,

	//    instead of recvfrom() and sendto()

	printf("Client: Client connecting \n");

	if (connect(conn_socket, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR)

	{

		fprintf(stderr, "Client: connect() failed: %d\n", WSAGetLastError());

		WSACleanup();

		return -1;

	}

	else

		printf("Client: connect() is OK.\n");



	// Test sending some string

	loopcount = 0;

	while (1)

	{
		char opcode = 0;
		int i;
#pragma warning(suppress : 4996)
		//sprintf(Buffer, "This is a test message from client #%d", loopcount++);
		COMM_HEADER *send_hdr = (COMM_HEADER *)Buffer;
		for (i = AIM_REC_BUFFER_RX_OPCODE; i < AIM_ACTIVATE_TX_CH; i++)
		{
			send_hdr->opcode2 = i;
 			send_hdr->opcode1 = OPCODE1_VERSION;
			send_hdr->datalenhigh = 0;
			send_hdr->datalenlow = (HEADER_LENGTH & 0x00FF);




			retval = send(conn_socket, Buffer, sizeof(Buffer), 0);

			if (retval == SOCKET_ERROR)

			{

				fprintf(stderr, "Client: send() failed: error %d.\n", WSAGetLastError());

				WSACleanup();

				return -1;

			}

			else

				printf("Client: send() is OK.\n");

			printf("Client: Sent data \"%s\"\n", Buffer);

		

		retval = recv(conn_socket, Buffer, sizeof(Buffer), 0);

		if (retval == SOCKET_ERROR)

		{

			fprintf(stderr, "Client: recv() failed: error %d.\n", WSAGetLastError());

			closesocket(conn_socket);

			WSACleanup();

			return -1;

		}

		else

			printf("Client: recv() is OK.\n");



		// We are not likely to see this with UDP, since there is no

		// 'connection' established.

		if (retval == 0)

		{

			printf("Client: Server closed connection.\n");

			closesocket(conn_socket);

			WSACleanup();

			return -1;

		}

	}
		

		printf("Client: Received %d bytes, data \"%s\" from server.\n", retval, Buffer);

		if (!loopflag)

		{

			printf("Client: Terminating connection...\n");

			break;

		}

		else

		{

			if ((loopcount >= maxloop) && (maxloop > 0))

				break;

		}

	}

	closesocket(conn_socket);

	WSACleanup();



	return 0;

}
