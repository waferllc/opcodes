/*
 *  opcodedrv.c
 *
 *  Description: driver abstrtions for different platforms
 *
 *  Created on: July 2, 2020
 *      Author: Sergey Uskach
 */


/* C Std Lib */
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 
#include <errno.h>

#include <unistd.h>

#include "opcodes.h"


extern void tcpip_task ( void *t_param );

int  clientfd;

int  bytesSent;
char buffer[TCPPACKETSIZE];
char tempchar;

char CommType;



/************************************************************************
 * Name       : RC main(void) /// SysCtlClockFreqSet
 * Description:
 * Arguments  :
 * Effected   :
 * Note(s)    :
 ************************************************************************/

int   main()
{
   

	
	T_Params  t_params;
	
	// Create a thread that will function threadFunc()
	//struct ThreadParams thread_params;
	memset ( &t_params, 0 , sizeof (T_Params) );
	
	t_params.port =  DEFAULT_PORT;
	strcpy ( t_params.ip_addr , DEFAULT_IP );
	
	tcpip_task (&t_params );
	 
    return (0);
}

void Communication(COMM_TYPE_DEFINE CommType, char Opcode, unsigned char *RecBuffer , int accepted_clientfd )
{

    //unsigned short  ctr;
	int rc = 0;
    //unsigned int  rb;    
    //unsigned short ctr2 = 0;
	
    

    static short  length;    
    
    MSG_HDR   *psHeader ;

    //unsigned short payload_len;

    
    printf("OpCode: %d 0x%x\n", Opcode ,Opcode);

    psHeader = (MSG_HDR *) RecBuffer;
	
    switch (Opcode)
    {
	case AIM_REC_BUFFER_RX_OPCODE :
    case AIM_REC_BUFFER_TX_OPCODE :
	case AIM_ACK_OPCODE :
	case AIM_JSON_OPCODES :	
	case AIM_DEBUG_RX_TX:
	case  AIM_SET_ZERO_TABLE :
	case  AIM_SET_CURVE_TABLE:
	case AIM_CHOOSE_ANTENNA:
	printf("opcode :0x%x line=%d file=%s \n", Opcode, __LINE__, __FILE__);       
    psHeader->OpCode2 = Opcode;
    psHeader->DataLen1 = 0;
    psHeader->DataLen2 = 0;
    rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
	break;
	
	
    
    case AIM_ACTIVATE_RX_CH:
    {
        
        
        printf("AIM_ACTIVATE_RX_CH: line=%d file=%s \n", __LINE__, __FILE__);
       
        psHeader->OpCode2 = AIM_ACTIVATE_RX_CH;
        psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
       
    }
    break;

    case AIM_ACTIVATE_TX_CH:
    {

        printf("AIM_ACTIVATE_TX_CH: line=%d file=%s \n", __LINE__, __FILE__);
        psHeader->OpCode2 = AIM_ACTIVATE_TX_CH ;
        psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
       
    }
    break;


    case AIM_FLASH_CALIB_CREATE:
    {
        

            psHeader->OpCode2 = AIM_FLASH_CALIB_CREATE;
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			printf("AIM_FLASH_CALIB_CREATE: line=%d file=%s \n", __LINE__, __FILE__);
            rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        
    }
    break;

    case AIM_FLASH_CALIB_STORE:
    {
       psHeader->OpCode2 =  AIM_FLASH_CALIB_STORE;
       psHeader->DataLen1 = 0;
	   psHeader->DataLen2 = 0;
	   printf("AIM_FLASH_CALIB_STORE: line=%d file=%s \n", __LINE__, __FILE__);
       rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
      
    }
    break;


    case AIM_FLASH_CALIB_RETRIEVE:
    {
            psHeader->OpCode2 = AIM_FLASH_CALIB_RETRIEVE;
            psHeader->OpCode1 = OPCODE1_VERSION;
            psHeader->DataLen1 =  0;
            psHeader->DataLen2 = 0;
			printf("AIM_FLASH_CALIB_RETRIEVE: line=%d file=%s \n", __LINE__, __FILE__);
           
       
    }
    break;

    case AIM_FLASH_CALIB_ERASE:
    {

       psHeader->OpCode2  = AIM_FLASH_CALIB_ERASE;
	   psHeader->DataLen1 =  0;
       psHeader->DataLen2 = 0;
           
       printf("AIM_FLASH_CALIB_ERASE: line=%d file=%s \n", __LINE__, __FILE__);
           
	   rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
    }
    break;
	
    case AIM_FULL_FLASH_ERASE:
	{
       psHeader->OpCode2 = AIM_FULL_FLASH_ERASE;
       psHeader->DataLen1 = 0;
	   psHeader->DataLen2 = 0;
       printf(" AIM_FULL_FLASH_ERASE: line=%d file=%s \n", __LINE__, __FILE__); 

       rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
	}

    case AIM_LOG_ERASE:
    {
        psHeader->OpCode2 = AIM_LOG_ERASE ;
		psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        
		printf(" AIM_LOG_ERASE: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);

    }
    break;

    case AIM_FLASH_LOG_CREATE:
    {
        psHeader->OpCode2 =  AIM_FLASH_LOG_CREATE;
		psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        printf(" AIM_FLASH_LOG_CREATE: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);       

    }
    break;

    case AIM_FLASH_LOG_START:
    {
      	psHeader->OpCode2 =  AIM_FLASH_LOG_START;
		psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        
		printf(" AIM_FLASH_LOG_START: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);

    }
    break;

    case AIM_LOG_RETRIEVE:
    {

        psHeader->OpCode2 = AIM_LOG_RETRIEVE;
        psHeader->DataLen1 =  0;
        psHeader->DataLen2 = 0;
    	printf(" AIM_LOG_RETRIEVE: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
    }

    break;

    case AIM_GET_FIRMWARE_VERSION:
        
        printf(" AIM_GET_FIRMWARE_VERSION   line = %d\n",  __LINE__);     

		psHeader->OpCode2 = AIM_GET_FIRMWARE_VERSION ;
        
        psHeader->DataLen1=  HEADER_LENGTH / 0xff;
        psHeader->DataLen2 =  HEADER_LENGTH % 0xff;
               
        //Send Track Ack.
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, (HEADER_LENGTH + length), accepted_clientfd);        
        break;
    case AIM_GOTO_BOOT:        
        //Set data length to zero when returning opcode ACK
        psHeader->OpCode2 = AIM_GOTO_BOOT;
        psHeader->DataLen1 = 0;
        psHeader->DataLen2 = 0;
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        
       	printf(" AIM_LGOTO_BOOT: line=%d file=%s \n", __LINE__, __FILE__); 

		break;
        //lock  angle in tx antenna.
    case AIM_LOCK_ANGLE_TX://Set voltage values to  the antenna according the recieve azimuth and elevation.
        //Set data length to zero when returning opcode ACK
		psHeader->OpCode2 = AIM_LOCK_ANGLE_TX;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf(" AIM_LOCK_ANGLE_TX: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        
        break;
        //lock  angle in rx antenna.
    case AIM_LOCK_ANGLE_RX://Set voltage values to  the antenna according the recieve azimuth and elevation.
        //Set data length to zero when returning opcode ACK

        psHeader->OpCode2 = AIM_LOCK_ANGLE_RX;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		
		printf(" AIM_LOCK_ANGLE_RX: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        
        break;
        //get antenna serial  number & revision and save in eeprom.
    case AIM_SET_ANTENNA_SERIALNUM_REV://each one max 16 chars

		psHeader->OpCode2 =  AIM_SET_ANTENNA_SERIALNUM_REV;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_SET_ANTENNA_SERIALNUM_REV: line=%d file=%s \n", __LINE__, __FILE__);
		      
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
		
        break;
    case AIM_GET_ANTENNA_SERIALNUM_REV:
        
		psHeader->OpCode2 =  AIM_GET_ANTENNA_SERIALNUM_REV;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_GET_ANTENNA_SERIALNUM_REV: line=%d file=%s \n", __LINE__, __FILE__);
		
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH + SERIAL_NUM_AND_REV_LEN, accepted_clientfd);
        
        break;
		
    case AIM_GET_ANT_INFO_STATUS://send Ant status to  the gui.

		psHeader->OpCode2 =  AIM_GET_ANT_INFO_STATUS;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_GET_ANT_INFO_STATUS: line=%d file=%s \n", __LINE__, __FILE__);        

        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd );
		break;
        
    case AIM_GET_SAT_STATUS:// send Sat status to  the gui.
    
        psHeader->OpCode2 =  AIM_GET_SAT_STATUS;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_GET_ANT_INFO_STATUS: line=%d file=%s \n", __LINE__, __FILE__);        

        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd );
        
        break;
    case AIM_SET_SAT_INFO:// recieve Sat parameters and save them.
        //Set data length to zero when returning opcode ACK
        psHeader->OpCode2 =  AIM_SET_SAT_INFO;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_SET_SAT_INFO: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        
        break;
        case AIM_AQUIRE_SAT:
			
        psHeader->OpCode2 =  AIM_AQUIRE_SAT;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_AQUIRE_SAT: line=%d file=%s \n", __LINE__, __FILE__); 
        rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
          
        break;
		
        case AIM_BUC_MUTE_UNMUTE:
        psHeader->OpCode2 =  AIM_BUC_MUTE_UNMUTE;
        psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        psHeader->DataLen2 = HEADER_LENGTH%0xff;
		printf("  AIM_BUC_MUTE_UNMUTE: line=%d file=%s \n", __LINE__, __FILE__); 
            rc = comm_pushTxBuffer(CommType,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        break;

		case  AIM_BUC_ATTEN :
        {
            psHeader->OpCode2 =  AIM_BUC_ATTEN;
        	psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        	psHeader->DataLen2 = HEADER_LENGTH%0xff;
			printf(" AIM_BUC_ATTEN: line=%d file=%s \n", __LINE__, __FILE__); 
            rc = comm_pushTxBuffer(CommType ,0, RecBuffer, HEADER_LENGTH ,accepted_clientfd);

        }
        break;
        case AIM_INITIATE_RX_PEAKING:

            //Set data length to zero when returning opcode ACK
            psHeader->OpCode2 =  AIM_INITIATE_RX_PEAKING;
        	psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        	psHeader->DataLen2 = HEADER_LENGTH%0xff;
			printf(" AIM_INITIATE_RX_PEAKIN line=%d file=%s \n", __LINE__, __FILE__);             
			rc = comm_pushTxBuffer(CommType ,0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            break;

			
        case AIM_GET_DESIRED_POSITION:
			
            psHeader->OpCode2 =  AIM_GET_DESIRED_POSITION;
        	psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        	psHeader->DataLen2 = HEADER_LENGTH%0xff;
			printf(" AIM_GET_DESIRED_POSITION line=%d file=%s \n", __LINE__, __FILE__);             
			
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            
            break;
        case AIM_SET_OPERATION_MODE:
            //Set data length to zero when returning opcode ACK
            psHeader->OpCode2 =  AIM_SET_OPERATION_MODE;
        	psHeader->DataLen1 = HEADER_LENGTH/ 0xff;
        	psHeader->DataLen2 = HEADER_LENGTH%0xff;
			printf(" AIM_SET_OPERATION_MODE line=%d file=%s \n", __LINE__, __FILE__);     
			rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            
            break;
			
        case AIM_SET_UNIFORM_RX_VOLTAGE:
            //Set data length to zero when returning opcode ACK
            psHeader->OpCode2 =  AIM_SET_UNIFORM_RX_VOLTAGE;
        	psHeader->DataLen2 = HEADER_LENGTH%0xff;
			printf(" AIM_SET_UNIFORM_RX_VOLTAGE line=%d file=%s \n", __LINE__, __FILE__);     
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            
            break;
        case AIM_SET_UNIFORM_TX_VOLTAGE:
            //Set data length to zero when returning opcode ACK
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_UNIFORM_TX_VOLTAGE;
			printf(" AIM_SET_UNIFORM_TX_VOLTAGE line=%d file=%s \n", __LINE__, __FILE__);             
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            
            break;
        case AIM_SET_MAGNETIC_DEC:
            //Set data length to zero when returning opcode ACK
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_MAGNETIC_DEC;
			printf(" AIM_SET_MAGNETIC_DEC line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            
            break;
        case AIM_GET_MAGNETIC_DEC:
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_GET_MAGNETIC_DEC;
			printf(" AIM_GET_MAGNETIC_DEC line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            break;

        case AIM_SET_PHASE_CURVE_TX:
        {
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_PHASE_CURVE_TX;
			printf(" AIM_SET_PHASE_CURVE_TX line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
        }
        break;
		
        case AIM_SET_PHASE_CURVE_RX_1:
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_PHASE_CURVE_RX_1;
			printf(" AIM_SET_PHASE_CURVE_RX_1 line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH, accepted_clientfd);
            break;
        case AIM_SET_PHASE_CURVE_RX_2:
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_PHASE_CURVE_RX_2;
			printf(" AIM_SET_PHASE_CURVE_RX_2 line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH , accepted_clientfd);
            break;
        case AIM_SET_PHASE_CURVE_RX_3:
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_SET_PHASE_CURVE_RX_3;
			printf(" AIM_SET_PHASE_CURVE_RX_3 line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH , accepted_clientfd);
            break;
        case AIM_DEBUG_OPCODE:
            psHeader->DataLen1 = 0;
            psHeader->DataLen2 = 0;
			psHeader->OpCode2 = AIM_DEBUG_OPCODE;
			printf(" AIM_DEBUG_OPCOD line=%d file=%s \n", __LINE__, __FILE__);   
            rc = comm_pushTxBuffer(CommType, 0, RecBuffer, HEADER_LENGTH , accepted_clientfd);
            break;


        default:
		rc = rc;
            break;
    }


}
