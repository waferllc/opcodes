# Copyright 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002
# Free Software Foundation, Inc.
# This Makefile.in is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.


ld_rpath = -Wl,-rpath /home/debian/fetcher
verified_header_path = /home/debian/fetcher/http_fetcher-1.1.0/include/http_fetcher.h
verified_library_path = /home/debian/fetcher/libhttp_fetcher.a
PROGRAMS = testfetch

SOURCES = testfetch.c

LDFLAGS = -Wl,-rpath /home/debian/fetcher
HEADERS = /home/debian/fetcher/http_fetcher-1.1.0/include


DEFAULT_INCLUDES =  -I. -I$(HEADERS)


COMPILE = $(CC) $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) $(CPPFLAGS)  $(CFLAGS)
CCLD = $(CC)
LINK = $(CCLD)  $(CFLAGS)  $(LDFLAGS) -o $@
CFLAGS =  -g -Wall -I/home/debian/fetcher/http_fetcher-1.1.0/include -L/home/debian/fetcher 




PROG = fetch
CC = gcc
CCC = g++
HTTPLIBPATH = /home/debian/fetcher/http_fetcher-1.1.0/src
CPPFLAGS = -I. -g -Wall   
LDADD =  -lhttp_fetcher

OBJS = fwupdater.o
OBJ_DATA = xmlparser.o

$(PROG)	: $(OBJS) $(OBJ_DATA) 
	$(CCC) $(LDFLAGS) -o $(PROG) $(OBJS) $(OBJ_DATA) -L$(HTTPLIBPATH) $(LDADD) 
 
$(OBJS) : fwupdater.c
	$(CC) $(DEFAULT_INCLUDES) -Wall -c fwupdater.c
	
$(OBJ_DATA) : xmlparser.cpp
	$(CC) $(CPPFLAGS) $(DEFAULT_INCLUDES) -c xmlparser.cpp

clean: 
	rm -f $(PROG) $(OBJS) $(OBJ_DATA) 
