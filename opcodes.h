/*
 *  opcodedrv.h
 *
 *  Description: driver abstrtions for different platforms
 *
 *  Created on: July 2, 2020
 *      Author: Sergey Uskach
 */

#ifndef OPCODEDRV_H
#define OPCODEDRV_H
#define TASKSTACKSIZE       4096
#define UDPPACKETSIZE       256
#define TCPPACKETSIZE 		1600
/* Errror Return Values */
#ifndef INVALID_SOCKET
#define INVALID_SOCKET (HANDLE)0xFFFFFFFF   /* Used by socket() and accept() */
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR   -1                   /* Used by the rest */
#endif



#ifdef __cplusplus  
extern "C" { 
#endif 
int connection_count;


#ifdef __cplusplus 
} 
#endif 
#define PREAMBLE_1 0x66
#define PREAMBLE_2 0x99
#define PREAMBLE_3 0x5A
#define PREAMBLE_4 0xA5
#define PREAMBLE_VAL1 0x6699         // ! Do not change (PC use)
#define PREAMBLE_VAL2 0x5AA5         // ! Do not change (PC use)
#define PREAMBLE32    ((PREAMBLE_VAL1 << 16) | PREAMBLE_VAL2)


#define AIM_CONT_ADDR             0x01

#define HEADER_LENGTH               12
#define SERIAL_NUM_AND_REV_LEN      32
#define SERIAL_NUM_LENGTH           16

// Opcode range for all bus types used by MCM
typedef enum
{
   // 0x00 & 0xFF used for Global Successive by all bus types
   ACK                            = 0x00,
   NACK                           = 0xFF,

}MSG_Commands;

#define OPCODE1_VERSION 2
typedef enum
{
    AIM_REC_BUFFER_RX_OPCODE = 0x50,    //0x50
    AIM_REC_BUFFER_TX_OPCODE,           //0x51
    AIM_ACK_OPCODE,                     //0x52
    AIM_JSON_OPCODES,                   //0x53
    AIM_GET_FIRMWARE_VERSION,           //0x54
    AIM_DEBUG_RX_TX,                    //0x56
    AIM_GOTO_BOOT,                      //0x56
    AIM_LOCK_ANGLE_RX,                  //0x57
    AIM_LOCK_ANGLE_TX,                  //0x58
    AIM_SET_ZERO_TABLE,                 //0x59
    AIM_SET_CURVE_TABLE,                //0x5a
    AIM_CHOOSE_ANTENNA,                 //0x5b
    AIM_ACTIVATE_RX_CH,                 //0x5c
    AIM_ACTIVATE_TX_CH,                 //0x5d
    AIM_EEPROM_WRITTE = 0x60,           //0x60
    AIM_EEPROM_READ,                    //0x61
    AIM_GET_ANTENNA_TYPE,               //0x62
    AIM_ANTENNA_PWM_FREQ_SET,           //0x63
    AIM_EEPROM_ERASE,                   //0x64
    AIM_SET_ANTENNA_SERIALNUM_REV,      //0x65
    AIM_GET_ANTENNA_SERIALNUM_REV,      //0x66
    AIM_SET_PWM_400HZ,                  //0x67
    AIM_SET_PWM_1000HZ,                 //0x68
    AIM_SET_PWM_PREVIOUS_VERSION,       //0x69
    AIM_SET_CALIBRATION_PARAMS,         //0x6a
    AIM_GET_RF_DETECTOR_VALUE_CONICAL,  //0x6b
    AIM_LOCK_TRACK_ANGLE,               //0x6c
    AIM_TRACK_SOME_CORDINATES,          //0x6d
    AIM_GET_RF_DETECTOR_VALUE,          //0x6e
    AIM_GET_ANT_INFO_STATUS,            //0x6f
    AIM_GET_SAT_STATUS,                 //0x70
    AIM_SET_SAT_INFO,                   //0x71
    AIM_AQUIRE_SAT,                     //0x72
    AIM_COMPASS_CAL_START,              //0x73
    AIM_COMPASS_CAL_STOP,               //0x74
    AIM_BUC_MUTE_UNMUTE,                //0x75
    AIM_INITIATE_RX_PEAKING,            //0x76
    AIM_GET_DESIRED_POSITION,           //0x77
    AIM_SET_OPERATION_MODE,             //0x78
    AIM_SET_UNIFORM_RX_VOLTAGE,         //0x79
    AIM_SET_UNIFORM_TX_VOLTAGE,         //0x7A
    AIM_SET_MAGNETIC_DEC,               //0x7B
    AIM_GET_MAGNETIC_DEC,               //0x7C
    AIM_SET_PHASE_CURVE_TX = 0x90,      //0x90
    AIM_SET_PHASE_CURVE_RX_1,           //0x91
    AIM_SET_PHASE_CURVE_RX_2,           //0x92
    AIM_SET_PHASE_CURVE_RX_3,           //0x93
    AIM_FULL_FLASH_ERASE,               //0x94
    AIM_LOG_ERASE,                     //0x95
    AIM_FLASH_LOG_CREATE,               //0x96
    AIM_LOG_START,                      //0x97
    AIM_LOG_RETRIEVE,                   //0x98
    AIM_FLASH_LOG_START,                //0x99
    AIM_FLASH_CALIB_CREATE,             //0x9a
    AIM_FLASH_CALIB_STORE,              //0x9b
    AIM_FLASH_CALIB_RETRIEVE,           //0x9c
    AIM_FLASH_CALIB_ERASE,              //0x9d
    AIM_BUC_ATTEN  ,                    //0x9f

    AIM_DEBUG_OPCODE       = 0xff,      //0xff
    ABUS_END_COMMANDS_LIST
} Abus_Commands;
typedef enum
{
    PREAMBLE1,       //0
    PREAMBLE2,       //1
    PREAMBLE3,       //2
    PREAMBLE4,       //3
    ADDRES1,         //4
    ADDRES2,         //5
    OPCODE1,         //6
    OPCODE2,         //7
    DATALEN1,        //8
    DATALEN2,        //9
    CHECKSUM1,       //10
    CHECKSUM2,       //11
    FIRST_DATA_INDEX//12
} MsgHeaderIndex;



typedef struct /* ! must be 32 bit aligned due to DSP limit */
{
   char Preamble1;
   char Preamble2;
   char Preamble3;
   char Preamble4;
   char Addr1;         // Dest & Src Addr
   char Addr2;         // Dest & Src Addr
   char OpCode1;       // OpCode & Resp_Code
   char OpCode2;       // OpCode & Resp_Code
   char DataLen1;
   char DataLen2;
   char CheckSum1;
   char CheckSum2;

}MSG_HDR;


typedef struct comm_header {
	unsigned char peambula1;
	unsigned char peambula2;
	unsigned char peambula3;
	unsigned char peambula4;
	unsigned char addr1;
	unsigned char addr2;
	unsigned char opcode1;
	unsigned char opcode2;
	unsigned char datalenhigh;
	unsigned char datalenlow;
	unsigned char crc1;
	unsigned char crc2;

} COMM_HEADER;

typedef struct 
{                                                                                                                                                                      
    int   port ;  
	char  ip_addr[24];  
	
}T_Params;     



typedef union fltasbyte 
{
    float fVal;
    int  intVal;
    unsigned long uintVal;
    unsigned char byteVal[4];
} floatAsByte;


void *task;
unsigned char *stack_size[TASKSTACKSIZE] ;


extern void *Semaphore_Handle;
extern void *driver_status;
typedef enum
{
	COMM_UART =1,
	COMM_USB,
	COMM_UDP,
	COMM_SPI,
	COMM_I2C,
	COMM_TCP,
	COMM_TYPE_END_LIST
}COMM_TYPE_DEFINE;

extern void Communication(COMM_TYPE_DEFINE CommType, char opcode , unsigned char * buffer, int socket);
extern int comm_pushTxBuffer(COMM_TYPE_DEFINE CommType, char DacNum, unsigned char *pStr, unsigned int length, int socket );
extern void Board_initGeneral(void);
extern void Board_initGPIO(void );
extern void Board_initEMAC(void);
extern void Board_initUSB(int param);
extern void IOports_Init(void);
    
extern void Board_initSPI(void );

extern void Board_initUART(void);
extern int OpenComport(char *comport, int baudrate);
extern void tcpWorker(unsigned int *arg0, unsigned int * arg1);


#ifndef DEFAULT_IP
#define DEFAULT_IP	"192.168.3.23"
#endif
#ifndef DEFAULT_PORT 
#define DEFAULT_PORT 10000
#endif


#endif
