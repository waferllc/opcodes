# Copyright 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002
# Free Software Foundation, Inc.
# This Makefile.in is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.


CC = gcc
CCC = g++

PROGRAMS = opcodesdrv

SOURCES = recvfunc.c systeminit.c opcodedrv.c
CFLAGS =  -g -Wall

DEFAULT_INCLUDES =  -I. 

COMPILE = $(CC)  $(DEFAULT_INCLUDES)  $(CPPFLAGS)  $(CFLAGS)
CCLD = $(CC)
LDFLAGS =-lpthread
CPPFLAGS = -I. -g -Wall   

OBJS = recvfunc.o systeminit.o opcodedrv.o


$(PROGRAMS)	: $(OBJS)  
	$(CC) $(LDFLAGS) -o $(PROGRAMS) $(OBJS) 
 
$(OBJS) : $(SOUCES)
	$(CC) $(DEFAULT_INCLUDES) -Wall -c $(SOURCES)


clean: 
	rm -f $(PROGRAMS) $(OBJS)  
